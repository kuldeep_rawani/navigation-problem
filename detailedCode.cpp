/**
 *   Code for Area Exploration using Navigation
 *   Copyright : J. Howlader
 *   Contributor : Debjit Mondal
 */


/**
 *   Header Files
 */
#include<stdio.h>
#include<math.h>

/**
 *   Number of Obstacles
 */
#define NO_OF_OBS 4

/**
 *   User-defined Arithmetic Functions
 */
float absdiff(float a, float b){
        if( a-b <0) return b-a;
        else return a-b;
}

float abs(float i){
        if(i<0) return -i;
        else return i;
}

/**
 *   Define coordinates
 */
class coordinate{
        public:
                float x,y;
                coordinate(float a,float b){
                        x=a;
                        y=b;
                }
                coordinate(){}
                void compute_force(coordinate x){}
                virtual float returnX(){return x;}
                virtual float returnY(){return y;}
};

/**
 *   Define the static nodes (Obstacles)
 */
class object: public coordinate{
        public:
                int flag;
                float force_x,force_y,amp,angle;
                float returnX(){return x;}
                float returnY(){return y;}
                float a,b;

                /**
                 *      Compute force from obstacle to moving body
                 */
                void compute_force(object* m){
                        force_x=(m->returnX()-x);
                        force_y=(m->returnY()-y);
                        amp=sqrt(pow(force_x,2)+pow(force_y,2));
                        angle=acos(force_x/amp);
                        a = amp*abs(sin(angle));
                        b = amp*abs(cos(angle));
                        printf("In compute_force: The amp force_x = %f,force_y  %f  \n",force_x,force_y);
                }

                float returnForce_X1(){ return force_x;}
                float returnForce_Y(){ return force_y;}
                object(float x,float y):coordinate(x,y){flag = 0;}
                object():coordinate(){flag = 0;}

                /**
                 *      Flags to indicate whether a node(obstacle) is already considered or not
                 */
                void setFlag(int f) {flag = f;}
                int returnFlag(){ return flag;}


                void display(){
                        //printf("The parameters :\n");
                } 
                float getX(){ return x;}
                float getY(){ return y;}
};

/**
 *  Define list of objects
 */
class list{
                public:
                object node;
                list* next;
                static list* head;
                list(object x){ node.x = x.x; node.y = x.y; next = NULL;}
                static void display(){
                        list* temp = head;
                        if(temp == NULL){
                                printf("List is empty\n");
                        }
                        while(temp != NULL){
                                 printf("coordinate (%f,%f) \n" ,temp->node.returnX(),temp->node.returnY());
                                 printf("Vector is (%f,%f) and angle %f \n", temp->node.a,temp->node.b,temp->node.angle);
                                temp = temp->next;
                        }
                         printf("\nList over\n");
                }
                void insert(){ next = head; head = this;}
                ~list(){
                        delete next;
                }
                static void emptied(object *arr){
                        static list *temp1,*temp = head;

                        if(temp->next!=NULL){ temp1=temp;  delete temp1; temp = temp->next;}
                        head = NULL;
                        for(int i=0;i<NO_OF_OBS;i++)
                                arr[i].setFlag(0);
                }
};


class moving:public object{
        public:
                moving(float x,float y):object(x,y){ }
                moving():object(){}

                /**
                 *      Never being used
                 */
                void compute_force(object* g){

                        force_x=g->x-x;
                        force_y=g->y-y;
                        amp=sqrt(pow(force_x,2)+pow(force_y,2));
                        angle=acos(force_x/amp);
                        force_x=amp*cos(angle);
                        force_y=amp*sin(angle); 
                         printf("Moving Object: compute_force (%f,%f)\n", force_x,force_y);
                }

                /**
                 *      Computes the area under consideration
                 */
                void compute_diera(object* arr, list *h){
                        printf("In compute_diera \n");
                        for(int i=0;i<NO_OF_OBS;i++){
                                arr[i].compute_force(this);                        /** same thing happening in the if statement below */
                        //      printf("Distance %f \n",absdiff(x,arr[i].x)+absdiff(y,arr[i].y));
                                if(absdiff(x,arr[i].x)+absdiff(y,arr[i].y)<=8){
                                        if(arr[i].returnFlag() == 0){
                        //                      printf("in");
                                                arr[i].setFlag(1);
                                                list* temp=new list(arr[i]);
                                                temp->node.compute_force(this);  // the forcf from obs to MB
                                                temp->insert();
                                        }
                                }
                        }
                }

                void result_force(list *s){
                        list* temp;
                        temp = s->head;
                        while(temp != NULL){
//                                 printf("The node in dira Force %f %f\n",temp->node.force_x,temp->node.force_y); 
                                force_x += temp->node.force_x;
                                force_y += temp->node.force_y;
                                temp = temp->next;
                        }
//                         printf("Resultany force %f %f",force_x,force_y);
                }
                void move(){ 
                        float angle = atan(force_y/force_x);
                        //printf("The current %d %d", x ,y);              
                        x =x+ 2*cos(angle);
                        y =y+ 2*sin(angle);
                        printf("%f %f\n",x,y);
                        //printf("The position of the body %f ,%f %d, %d", 2*cos(angle),2*sin(angle), x,y);       
                }
};

list* list::head = NULL;

int main(){

        object array[NO_OF_OBS];
        list* start;

        /**
         *  Coordinates of obstacles are taken as input
         */
        for(int i=0;i<NO_OF_OBS;i++){
                float x,y;
                scanf("%f %f",&x,&y);
                array[i] = object(x,y);
        }
        getchar();

        /**
         *  Starting position of the moving body
         */
        moving body = moving(5,0);

        /**
         *  Position of the goal
         */
        object goal = object(20, 15);

        int count =0;
        while(count <100){
                printf("Iteration %d comp_dira\n", count);
                body.compute_diera(array,start);
                printf("display \n");
                list::display();
                printf("resultant force \n");
                body.result_force(start);
                float tempx = goal.getX()-body.getX();
                float tempy = goal.getY()-body.getY();
                float tempamp = sqrt(pow(tempx,2)+pow(tempy,2));
                float tempangle=acos(tempx/tempamp);
                body.force_x+=10.0*cos(tempangle);
                body.force_y+=10.0*sin(tempangle); 
                body.move();
                body.force_x = 0;
                body.force_y = 0;
                list::emptied(array);
                if((body.getY() >= goal.getY())&&(body.getX() >= goal.getX())) break;
                count++;
                }
}
