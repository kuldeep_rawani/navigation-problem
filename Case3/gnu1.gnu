set term wxt enhanced
set title 'Path of moving body'
set xlabel 'X - Coordinate'
set ylabel 'Y - Coordinate' 
plot [:30][:20] 'Case3output.out'w line title 'Path','Case3input.in' title 'Obs','goal.txt' title 'Goal'
set term postscript eps enhanced 'Helvetica' 24
set output 'Case3graph.eps'
replot
