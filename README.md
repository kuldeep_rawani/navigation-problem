Navigation Problem and Iterative Path Enhancement : In this problem we have to get an optimized path for a moving object in a plane which consists of stationary obstacles. While moving the object has to take the path such that it avoids the obstacles that comes in way of it and deviates its path to reach the destination

Programming language used: C++
