// TODO: Documentation

#include<stdio.h>
#include<math.h>
#define NO_OF_OBS 4
float absdiff(float a, float b){
        if( a-b <0) return b-a;
        else return a-b;
}

float abs(float i){
        if(i<0) return -i;
        else return i;
}

/**
    Defines coordinates
 */

class coordinate{
  
        public:
                float x,y;
                coordinate(float a,float b){
                        x=a;
                        y=b;
                }
                coordinate(){}
                void compute_force(coordinate x){}
                virtual float returnX(){return x;}
                virtual float returnY(){return y;}
};



class object: public coordinate{
        public:
                int flag;
                float force_x,force_y,amp,angle;
                float returnX(){return x;}
                float returnY(){return y;}
                float a,b;
                void compute_force(object* m){
                        force_x=(m->returnX()-x);
                        force_y=(m->returnY()-y);
                        amp=sqrt(pow(force_x,2)+pow(force_y,2));
                        angle=acos(force_x/amp);
                        a = amp*abs(sin(angle));
                        b = amp*abs(cos(angle));
//                         printf("In compote_force: The amp force_x = %f,force_y  %f  \n",force_x,force_y);
                }
                float returnForce_X1(){ return force_x;}
                float returnForce_Y(){ return force_y;}
                object(float x,float y):coordinate(x,y){flag = 0;}
                object():coordinate(){flag = 0;}
                void setFlag(int f) {flag = f;}
                int returnFlag(){ return flag;}
                void display(){
                        //printf("The parameters :\n");
                } 
                float getX(){ return x;}
                float getY(){ return y;}
};



class list{
                public:
                object node;
                list* next;
                static list* head;
                list(object x){ node.x = x.x; node.y = x.y; next = NULL;}
                static void display(){
                        list* temp = head;
                        if(temp == NULL){
//                                 printf("List is empty\n");
                        }
                        while(temp != NULL){
//                                  printf("coordinate (%f,%f) \n" ,temp->node.returnX(),temp->node.returnY());
//                                  printf("Vector is (%f,%f) and angle %f \n", temp->node.a,temp->node.b,temp->node.angle);
                                temp = temp->next;
                        }
//                          printf("\nList over\n");
                }
                void insert(){ next = head; head = this;}
                ~list(){
                        delete next;
                }
                static void emptied(object *arr){
                        static list *temp1,*temp = head;

                        if(temp->next!=NULL){ temp1=temp;  delete temp1; temp = temp->next;}
                        head = NULL;
                        for(int i=0;i<NO_OF_OBS;i++)
                                arr[i].setFlag(0);
                }
};


class moving:public object{
        public:
                moving(float x,float y):object(x,y){ }
                moving():object(){}
                void compute_force(object* g){

                        force_x=g->x-x;
                        force_y=g->y-y;
                        amp=sqrt(pow(force_x,2)+pow(force_y,2));
                        angle=acos(force_x/amp);
                        force_x=amp*cos(angle);
                        force_y=amp*sin(angle); 
//                          printf("Moving Object: compute_force (%f,%f)\n", force_x,force_y);
                }
                void compute_diera(object* arr, list *h){
//                         printf("In compute_diera \n");
                        for(int i=0;i<NO_OF_OBS;i++){
                                arr[i].compute_force(this);
                        //      printf("Distance %f \n",absdiff(x,arr[i].x)+absdiff(y,arr[i].y));
                                if(absdiff(x,arr[i].x)+absdiff(y,arr[i].y)<=8){
                                        if(arr[i].returnFlag() == 0){
                        //                      printf("in");
                                                arr[i].setFlag(1);
                                                list* temp=new list(arr[i]);
                                                temp->node.compute_force(this);  // the forcf from obs to MB
                                                temp->insert();
                                        }
                                }
                        }
                }

                void result_force(list *s){
                        list* temp;
                        temp = s->head;
                        //getchar();
                        while(temp != NULL){
//                                 printf("The node in dira Force %f %f\n",temp->node.force_x,temp->node.force_y); 
                                force_x += temp->node.force_x;
                                force_y += temp->node.force_y;
                                temp = temp->next;
                        }
//                         printf("Resultany force %f %f",force_x,force_y);
                }
                void move();
};

moving body = moving(5,0);
object goal = object(20, 15);

void moving::move(){ 
                        float angle = atan(force_y/force_x);
                        //printf("The current %d %d", x ,y);
                        x =x+ 2*cos(angle);
                        y =y+ 2*sin(angle);
                        /*
                         TODO : max x= goal.x , max y = goal.y
                        **/
                        if(x>goal.getX())
                            x=goal.getX();
                        if(y>goal.getY())
                            y=goal.getY();
                        printf("%f %f\n",x,y);
                        //printf("The position of the body %f ,%f %d, %d", 2*cos(angle),2*sin(angle), x,y);       
                }

list* list::head = NULL;

int main(){

        object array[NO_OF_OBS];
        list* start;

        for(int i=0;i<NO_OF_OBS;i++){
                float x,y;
                //printf("The position of the obst: (x,y)");
                scanf("%f %f",&x,&y);
                array[i] = object(x,y);
        }
//          printf("OK");
        getchar();
        //printf("The position of the moving body");
        int count =0;
        while(count <150){
                if((body.getY() >= goal.getY())&&(body.getX() >= goal.getX())) break;
//                 printf("Iteration %d comp_dira\n", count);
                body.compute_diera(array,start);
//                 printf("display \n");
                list::display();
//               printf("OK");
//                 printf("resultant force \n");
                body.result_force(start);
                float tempx = goal.getX()-body.getX();
                float tempy = goal.getY()-body.getY();
                float tempamp = sqrt(pow(tempx,2)+pow(tempy,2));
                float tempangle=acos(tempx/tempamp);
                body.force_x+=10.0*cos(tempangle);
                body.force_y+=10.0*sin(tempangle); 
                body.move();
                body.force_x = 0;
                body.force_y = 0;
                list::emptied(array);
                count++;
                }
}
