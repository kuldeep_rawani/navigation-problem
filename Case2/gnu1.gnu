set term wxt enhanced
set title 'Path of moving body'
set xlabel 'X - Coordinate'
set ylabel 'Y - Coordinate' 
plot [:30][:20] 'project.out'w line title 'optimized', 'Case2output.out'w line title 'Path','Case2input.in' title 'Obs','goal.txt' title 'Goal'
set term postscript eps enhanced 'Helvetica' 24
set output 'Compare.eps'
replot
