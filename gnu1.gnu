set term wxt enhanced
set title 'Path of moving body'
set xlabel 'X - Coordinate'
set ylabel 'Y - Coordinate' 
plot [:300][:200] 'out.txt'w line title 'Path','inp.txt' title 'Obs','goal.txt' title 'Goal'
set term postscript eps enhanced 'Helvetica' 24
set output 'Testgraph.eps'
replot
